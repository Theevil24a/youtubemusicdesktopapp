#!/bin/sh

mkdir ~/.local/share/YoutubeMusic
cp -Rf * ~/.local/share/YoutubeMusic
cp -Rf ~/.local/share/YoutubeMusic/installer/YoutubeMusic.desktop ~/.local/share/applications/
exit
